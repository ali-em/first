import fetch from 'isomorphic-unfetch';
import { api } from '../pages/config/index';
import { useRouter } from 'next/router';
const DeleteShop = (props) => {
	const Router = useRouter();
	const deleteShop = async () => {
		const res = await fetch(`${api}/market/shop/${props.name}`, {
			method: 'DELETE',
			headers: {
				token: localStorage.getItem('token'),
			},
		});
		const data = await res.json();
		if (data.status == 'ok') {
			alert('حذف شد');
			Router.push('/');
		} else {
			if (data.status == 'error') alert(data.data);
		}
	};
	return (
		<button
			className="waves-effect waves-light btn-small red"
			onClick={deleteShop}
		>
			حذف فروشگاه
		</button>
	);
};

export default DeleteShop;
