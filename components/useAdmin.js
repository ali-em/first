import { useEffect, useState } from 'react';
import { api } from '../pages/config';

const useAdmin = () => {
	const [hasRole, setHasRole] = useState(false);
	const [role, setRole] = useState(null);
	const checkLogin = async (cb) => {
		const token = localStorage.getItem('token');
		if (token == undefined) {
			setHasRole(false);
			setRole(null);
			return;
		}

		const res = await fetch(`${api}/user/getrole`, {
			headers: {
				token: token,
			},
		});

		const role = (await res.json()).role;
		if (role == 'admin' || role == 'superadmin') {
			setHasRole(true);
			setRole(role);
			return;
		}

		setHasRole(false);
		setRole(null);
		return;
	};
	useEffect(() => {
		checkLogin();
	}, []);
	return [hasRole, role];
};
export default useAdmin;
