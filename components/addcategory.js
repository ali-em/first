import fetch from 'isomorphic-unfetch';
import { server } from '../pages/config/index';
const AddCategory = (props) => {
	const createCategory = async () => {
		const value = document.getElementById('category').value;
		const body = JSON.stringify({
			name: value,
		});
		console.log(body);
		const res = await fetch(`${server}/api/v1/market/category`, {
			method: 'POST',
			body,
			headers: {
				'Content-Type': 'application/json',
				token: localStorage.getItem('token'),
			},
		});
		const data = await res.json();
		if (data.status == 'ok') {
			alert('Added successfully');
		} else {
			alert(`Error ${data.data.errors[0].message}`);
		}
	};
	return (
		<div
			style={{
				margin: 'auto',
				marginTop: 20,
				width: '50%',
				boxShadow: '1px 1px 3px 1px rgba(0,0,0,0.5)',
				padding: '20px 30px',
				boxSizing: 'content-box',
			}}
		>
			<h5 className="center-align">دسته بندی جدید</h5>

			<div className="input-field col m6 s10 offset-s1 ">
				<input id="category" type="text" required />
				<label htmlFor="category">نام دسته بندی</label>
			</div>

			<button
				role="button"
				onClick={createCategory}
				className="waves-effect waves-light btn col s6 m2 "
			>
				ثبت دسته بندی
			</button>
		</div>
	);
};

export default AddCategory;
