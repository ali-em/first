import Link from 'next/link';
const style = () => (
	<style jsx global>{`
		#nav {
			width: 100%;
			padding: 20px;
			box-sizing: border-box;
			box-shadow: 0 1px 10px 1px rgba(0, 0, 0, 0.5);
		}
		.navItem {
			text-decoration: none;
			color: inherit;
			padding: 0px 20px;
		}
	`}</style>
);
const Header = () => (
	<div id="nav">
		{style()}
		<Link href="/">
			<a className="navItem">خانه </a>
		</Link>
		<Link href="/category">
			<a className="navItem"> دسته بندی </a>
		</Link>
		<Link href="/contact">
			<a className="navItem">تماس با ما</a>
		</Link>
	</div>
);

export default Header;
