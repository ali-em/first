import fetch from 'isomorphic-unfetch';
import { api } from '../pages/config/index';
import { useRouter } from 'next/router';
const EditShop = (props) => {
	const Router = useRouter();
	const editShop = () => {
		Router.push(`/admin/editshop/${props.name}`);
	};
	return (
		<button
			className="waves-effect waves-light btn-small"
			onClick={editShop}
		>
			تغییر اطلاعات فروشگاه
		</button>
	);
};

export default EditShop;
