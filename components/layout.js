import Header from './header';
import Head from 'next/head';
import globalStyles from '../styles/global.js';

const Layout = (props) => (
	<div className="layout">
		<Head>
			<link rel="stylesheet" href="/materialize.min.css" />
			<link rel="stylesheet" href="/style.css" />
			<script src="/materialize.min.js"></script>
		</Head>
		<style jsx global>
			{globalStyles}
		</style>
		<Header />
		{props.children}
	</div>
);

export default Layout;
