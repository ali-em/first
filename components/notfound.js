import { useRouter } from 'next/router';

const NotFound = ({ item }) => {
	const router = useRouter();
	return (
		<div className="notfound-wrapper">
			<div style={{ position: 'absolute', top: '50%' }}>
				{item} مورد نظر پیدا نشد
				<p>
					<a href="#" onClick={router.back}>
						بازگشت به صفحه ی قبل
					</a>
				</p>
			</div>
		</div>
	);
};

export default NotFound;
