const sequelize = require('./src/models/config');
const { User } = require('./src/models/Account');
const app = require('./src/app');
sequelize
	.sync({
		// force: true,
	})
	.then(() => {
		User.create({
			username: 'superadmin',
			password: 'superadmin',
			role: 'superadmin',
			email: 'emami1999@gmail.com',
		})
			.then((user) => console.log('created'))
			.catch((err) => console.log('not created'));
	});

app.listen(8000, () => {
	console.log('server is up on :8000');
});
