const supertest = require('supertest');
const app = require('../app');
const db = require('../models/config');
beforeAll(async () => {
	await db.sync({ force: true });
});
describe('Testing auth', () => {
	var token;
	it('tests signup api', async () => {
		const response = await supertest(app).post('/api/v1/user/signup').send({
			username: 'ali',
			email: 'a@gmail.com',
			password: '1234',
		});
		token = response.body.data.token;
		expect(response.status).toBe(200);
		expect(response.body.status).toBe('ok');
		expect(response.body.data).toHaveProperty('token');
	});

	it('tests login api', async () => {
		var response = await supertest(app).post('/api/v1/user/login').send({
			usernameOrEmail: 'ali',
			password: '1234',
		});
		expect(response.status).toBe(200);
		expect(response.body.status).toBe('ok');
		expect(response.body.data).toHaveProperty('token');

		var response = await supertest(app).post('/api/v1/user/login').send({
			usernameOrEmail: 'ali',
			password: '12345',
		});
		expect(response.status).toBe(404);
		expect(response.body.status).toBe('error');
		expect(response.body.reason).toBe('wrong password');

		var response = await supertest(app).post('/api/v1/user/login').send({
			usernameOrEmail: 'ali2',
			password: '1234',
		});
		expect(response.status).toBe(404);
		expect(response.body.status).toBe('error');
		expect(response.body.reason).toBe('no such a username/email');
	});
	it('tests loggedin middleware', async () => {
		var response = await supertest(app)
			.get('/api/v1/user/loggedin')
			.set('token', token);
		expect(response.status).toBe(200);

		var response = await supertest(app)
			.get('/api/v1/user/loggedin')
			.set('token', '1234');
		expect(response.status).toBe(403);
	});
});

afterAll(async () => {
	await db.close();
});
