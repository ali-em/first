const router = require('express').Router();
const multer = require('multer');
const path = require('path');
const _ = require('lodash');
const { Product, Shop, Category, CatShop, Image } = require('../models/Market');
const {
	createShop,
	createProduct,
	editShop,
} = require('../models/Market/marketMethods');
const { isLoggedIn } = require('../middlewares/auth');

const storage = multer.diskStorage({
	destination: './public/images/',
	filename: function (req, file, cb) {
		cb(null, 'IMAGE-' + Date.now() + path.extname(file.originalname));
	},
});
const upload = multer({
	storage: storage,
	limits: { fileSize: 10000000 },
});
router.get('/shops/category', async (req, res) => {
	const categories = await Category.findAll();
	res.send(categories);
});
router.post('/category', (req, res) => {
	var data = req.body;
	console.log(data);
	Category.create({
		name: data.name,
	})
		.then((cat) => res.send({ status: 'ok', data: cat }))
		.catch((err) => res.send({ status: 'error', data: err }));
});
router.post(
	'/shop/:id/product',
	isLoggedIn(['admin', 'superadmin']),
	upload.array('images'),
	(req, res) => {
		req.body.images = req.files.map((file) => file.filename);

		Shop.findOne({ where: { id: req.params.id } })
			.catch((e) =>
				res
					.status(404)
					.send({ status: 'error', data: 'no such a shop' })
			)
			.then((shop) => createProduct(req.body, shop))
			.then((product) => res.send({ status: 'ok', data: product }))
			.catch((err) => res.send({ status: 'error', error: err }));
	}
);
router.delete(
	'/shop/:name',
	isLoggedIn(['admin', 'superadmin']),
	(req, res) => {
		Shop.findOne({ where: { name: req.params.name } })
			.then((shop) => {
				shop.destroy()
					.then(() => res.send({ status: 'ok' }))
					.catch((err) => res.send({ status: 'error', data: err }));
			})
			.catch((err) =>
				res.send({ status: 'error', data: 'no such a shop' })
			);
	}
);
router.patch(
	'/shop/:name',
	isLoggedIn(['admin', 'superadmin']),
	upload.single('image'),

	async (req, res) => {
		req.body.image = undefined;
		if (req.file) req.body.image = '/images/' + req.file.filename;
		editShop(req.params.name, req.body)
			.then((shop) => res.send({ status: 'ok', data: shop.dataValues }))
			.catch((err) => res.send({ status: 'error', data: err }));
	}
);
router.post(
	'/shop',
	isLoggedIn(['admin', 'superadmin']),
	upload.single('image'),
	(req, res) => {
		req.body.image = undefined;
		if (req.file) req.body.image = '/images/' + req.file.filename;
		createShop(req.body)
			.then((shop) => res.send({ status: 'ok', data: shop.dataValues }))
			.catch((err) => res.send({ status: 'error', data: err }));
	}
);
router.get('/shops/:name', (req, res) => {
	Shop.findOne({
		where: { name: req.params.name },
		include: [
			{
				model: Product,
				as: 'products',
				attributes: { exclude: [] },
				include: [
					{
						model: Image,
						as: 'images',
					},
				],
			},
			{
				model: Image,
				as: 'image',
			},
		],
	})
		.then((products) => {
			if (products != null) res.send({ status: 'ok', data: products });
			else res.send({ status: 'error', data: 'not_found' });
		})
		.catch((err) => res.send({ status: 'error', data: 'not_found' }));
});
router.get('/shops/:name/product/:id', (req, res) => {
	Shop.findOne({
		where: { name: req.params.name },
		include: [
			{
				model: Product,
				as: 'products',
				where: {
					id: req.params.id,
				},
				attributes: { exclude: [] },
				include: [
					{
						model: Image,
						as: 'images',
					},
				],
			},
		],
	})
		.then((products) => {
			res.send(products);
		})
		.catch((err) => res.send({ status: 'error', data: 'not_found' }));
});
router.get('/shops/:id/products', (req, res) => {
	Shop.findOne({
		where: { id: req.params.id },
		include: [
			{
				model: Product,
				as: 'products',
				attributes: { exclude: [] },
				include: [
					{
						model: Image,
						as: 'images',
					},
				],
			},
		],
	})
		.then((products) => {
			res.send(products);
		})
		.catch((err) => res.send({ status: 'error', data: 'not_found' }));
});
router.get('/shops', (req, res) => {
	Shop.findAll({
		include: [
			{
				model: Category,
				as: 'categories',
				required: false,
				attributes: ['id', 'name'],
				through: {
					model: CatShop,
					as: 'catshop',
					attributes: [],
				},
			},
			{
				model: Image,
				as: 'image',
				required: false,
				attributes: ['dest'],
			},
		],
	}).then((shops) => {
		res.send(shops);
	});
});

router.get('/shops/region/:num', (req, res) => {
	Shop.findAll({
		where: {
			region: req.params.num,
		},
		include: [
			{
				model: Category,
				as: 'categories',
				required: false,
				attributes: ['id', 'name'],
				through: {
					model: CatShop,
					as: 'catshop',
					attributes: [],
				},
			},
		],
	}).then((shops) => {
		res.send(shops);
	});
});

router.get('/shops/category/:id', (req, res) => {
	Category.findOne({
		where: { id: req.params.id },
		include: [
			{
				model: Shop,
				as: 'shops',
				required: false,
				attributes: {
					exclude: ['userId'],
				},
				through: {
					model: CatShop,
					as: 'catshop',
					attributes: [],
				},
			},
		],
	}).then((data) => {
		res.send({ status: data == null ? 'error' : 'ok', data: data });
	});
});
module.exports = router;
