const express = require('express');
const AccountMethods = require('../models/Account/accountMethods');
const { isLoggedIn } = require('../middlewares/auth');
const { isProfileCompleted } = require('../middlewares/profile');
const router = express.Router();

router.post('/signup', isLoggedIn(['superadmin']), (req, res) => {
	var data = req.body;
	if (!['user', 'admin'].includes(data.role)) data.role = 'user';
	AccountMethods.createUser(data)
		.then((r) => {
			res.send({ status: 'ok', data: r });
		})
		.catch((err) => res.send({ status: 'error', data: err }));
});
router.get(
	'/getrole',
	isLoggedIn(['user', 'admin', 'superadmin']),
	(req, res) => {
		res.send({ role: req.loggedInUser.role });
	}
);
router.post('/login', (req, res) => {
	const data = req.body;
	AccountMethods.loginUser(data)
		.then((e) => res.send(e))
		.catch((err) => res.status(404).send(err));
});

router.get('/loggedin', isLoggedIn(['user']), (req, res) => {
	console.log(req.loggedInUser);
	res.send(req.loggedInUser);
});

router.get('/profile', isProfileCompleted, (req, res) => {
	res.send(req.loggedInUserProfile);
});
module.exports = router;
