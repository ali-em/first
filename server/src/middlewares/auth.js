const { User } = require('../models/Account');
const { Responses } = require('./consts');
isLoggedIn = (accepts, excepts) => async (req, res, next) => {
	const token = req.headers.token;
	const user = await User.getUserByToken(token);
	if (
		user != null &&
		accepts.includes(user.role) &&
		(excepts == undefined || !excepts.includes(user.role))
	) {
		req.loggedInUser = user;
		next();
	} else {
		return res.status(403).send(Responses.unAuthorized(accepts));
	}
};

module.exports = {
	isLoggedIn,
};
