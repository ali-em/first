module.exports = {
	Responses: {
		unAuthorized: (shouldBe) => ({
			status: 'unAuthorized',
			shouldBe,
		}),
		inCompleteProfile: () => ({
			status: 'inCompleteProfile',
		}),
	},
};
