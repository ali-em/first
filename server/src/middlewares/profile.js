const { User, Profile } = require('../models/Account');
const { Responses } = require('./consts');
isProfileCompleted = async (req, res, next) => {
	const token = req.headers.token;
	const user = await User.getUserByToken(token);
	if (user == null)
		return res.status(403).send(Responses.unAuthorized(['user']));
	var isCompleted = true;
	const profile = await user.getProfile();
	Object.values(profile.dataValues).forEach((atr) => {
		if (atr == null) {
			isCompleted = false;
			return;
		}
	});
	console.log(isCompleted);
	if (isCompleted == false)
		return res.status(403).send(Responses.inCompleteProfile());
	else {
		req.loggedInUser = user;
		req.loggedInUserProfile = profile;
		next();
	}
};
module.exports = {
	isProfileCompleted,
};
