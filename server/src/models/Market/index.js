const sequelize = require('../config');
const Sequelize = require('sequelize');
const Product = require('./product');
const Shop = require('./shop');
const User = require('../Account/user');
const Category = require('./category');
const Image = require('./image');
var image = Image(sequelize);
var category = Category(sequelize);
var product = Product(sequelize);
var shop = Shop(sequelize);
var user = User(sequelize);
const catshop = sequelize.define('catshop', {
	shopId: {
		type: Sequelize.INTEGER,
		references: {
			model: shop,
			key: 'id',
		},
	},
	categoryId: {
		type: Sequelize.INTEGER,
		references: {
			model: category, // 'Actors' would also work
			key: 'id',
		},
	},
});
shop.hasMany(product);
shop.hasOne(image);
image.belongsTo(shop);

product.hasMany(image);
image.belongsTo(product);
shop.belongsToMany(category, {
	through: 'catshop',
	as: 'categories',
	foreignKey: 'shopId',
	otherKey: 'categoryId',
});
category.belongsToMany(shop, {
	through: 'catshop',
	as: 'shops',
	foreignKey: 'categoryId',
	otherKey: 'shopId',
});
shop.belongsTo(user);
product.belongsTo(shop);

module.exports = {
	Shop: shop,
	Product: product,
	Category: category,
	CatShop: catshop,
	Image: image,
};
