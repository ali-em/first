const Sequelize = require('sequelize');

const Model = Sequelize.Model;
class Shop extends Model {}

schema = (sequelize) =>
	Shop.init(
		{
			name: {
				type: Sequelize.STRING,
				allowNull: false,
				unique: true,
			},
			description: {
				type: Sequelize.TEXT,
				defaultValue: '',
			},
			contact: {
				type: Sequelize.TEXT,
				defaultValue: '',
			},
			phoneNumber: {
				type: Sequelize.TEXT,
				defaultValue: '',
			},
			region: {
				type: Sequelize.STRING,
				defaultValue: 'not set',
			},
		},
		{ sequelize, modelName: 'shop' }
	);

module.exports = schema;
