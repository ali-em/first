const Sequelize = require('sequelize');

const Model = Sequelize.Model;
class Category extends Model {}
schema = (sequelize) =>
	Category.init(
		{
			name: {
				type: Sequelize.STRING,
				allowNull: false,
				unique: true,
			},
		},
		{ sequelize, modelName: 'category' }
	);

module.exports = schema;
