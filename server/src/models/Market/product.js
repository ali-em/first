const Sequelize = require('sequelize');

const Model = Sequelize.Model;
class Product extends Model {}
schema = (sequelize) =>
	Product.init(
		{
			name: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			description: {
				type: Sequelize.TEXT,
				defaultValue: '',
			},
			price: {
				type: Sequelize.DECIMAL,
				allowNull: false,
			},
			outOfSale: {
				type: Sequelize.BOOLEAN,
				defaultValue: false,
			},
		},
		{ sequelize, modelName: 'product' }
	);

module.exports = schema;
