const { Shop, Product, Category, Image } = require('./index');

const _ = require('lodash');
createShop = (data) => {
	var categories = data.categories;
	var imagePath = data.image;
	var shopData = _.pick(data, [
		'name',
		'description',
		'phoneNumber',
		'contact',
		'address',
		'region',
	]);
	return Shop.create(shopData)
		.catch((e) =>
			Promise.reject(
				e.errors.map((error) => ({
					field: error.path,
					reason: error.validatorKey,
				}))
			)
		)
		.then(async (shop) => {
			if (categories != undefined) {
				caterogies = await Category.findAll({
					where: { id: categories },
				});
				await shop.setCategories(categories);
			}
			var image = null;
			if (imagePath != undefined) {
				image = await Image.create({ dest: imagePath });
				await shop.setImage(image);
			}
			return Promise.resolve(shop);
		})
		.catch((e) => {
			if (!e.errors) return Promise.reject(e);
			Promise.reject(
				e.errors.map((error) => ({
					field: error.path,
					reason: error.validatorKey,
				}))
			);
		});
};
const editShop = async (name, data) => {
	var categories = data.categories;
	var imagePath = data.image;
	var shopData = _.pick(data, [
		'name',
		'description',
		'phoneNumber',
		'contact',
		'address',
		'region',
	]);

	return Shop.update(shopData, { where: { name: name } })
		.catch((e) => {
			console.log(e);
			return Promise.reject(
				e.errors.map((error) => ({
					field: error.path,
					reason: error.validatorKey,
				}))
			);
		})
		.then(async () => {
			let shop = await Shop.findOne({ where: { name: shopData.name } });
			if (categories != undefined) {
				caterogies = await Category.findAll({
					where: { id: categories },
				});
				await shop.setCategories(categories);
			}
			var image = null;
			if (imagePath != undefined) {
				image = await Image.create({ dest: imagePath });
				await shop.setImage(image);
			}
			return Promise.resolve(shop);
		})
		.catch((e) => {
			console.log(e);
			if (!e.errors) return Promise.reject(e);
			Promise.reject(
				e.errors.map((error) => ({
					field: error.path,
					reason: error.validatorKey,
				}))
			);
		});
};
const createProduct = (data, shop) => {
	images = data.images.map((filename) => ({ dest: filename }));
	data = _.pick(data, ['name', 'description', 'price']);
	return Product.create(data)
		.then(async (product) => {
			await product.setShop(shop);
			if (images != undefined) {
				images = await Image.bulkCreate(images);
				await product.setImages(images);
			}
			return Promise.resolve(product);
		})
		.catch((err) => Promise.reject(err));
};
module.exports = { createShop, createProduct, editShop };
