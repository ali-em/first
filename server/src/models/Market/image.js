const Sequelize = require('sequelize');

const Model = Sequelize.Model;
class Image extends Model {}
schema = (sequelize) =>
	Image.init(
		{
			dest: {
				type: Sequelize.TEXT,
				allowNull: false,
			},
		},
		{ sequelize, modelName: 'image' }
	);

module.exports = schema;
