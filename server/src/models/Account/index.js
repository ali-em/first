const sequelize = require('../config');
const user = require('./user');
const profile = require('./profile');
const { Shop } = require('../Market');
User = user(sequelize);
Profile = profile(sequelize);
User.hasOne(Profile);
User.hasOne(Shop);
Profile.belongsTo(User);
module.exports = {
	User,
	Profile,
};
