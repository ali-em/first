const Sequelize = require('sequelize');
const Model = Sequelize.Model;

class Profile extends Model {}

module.exports = (sequelize) =>
	Profile.init(
		{
			name: {
				type: Sequelize.STRING,
				allowNull: true,
			},
			phoneNumber: {
				type: Sequelize.STRING,
				allowNull: true,
			},
			address: {
				type: Sequelize.STRING,
				allowNull: true,
			},
		},
		{ sequelize, modelName: 'profile' }
	);
