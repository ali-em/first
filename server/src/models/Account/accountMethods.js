const { User, Profile } = require('./index');
const _ = require('lodash');

const visibleProps = (user) => _.pick(user, ['id', 'username', 'email']);
const createUser = (data) => {
	const errors = User.signupIsValid(data);
	if (errors.length == 0)
		return User.create({
			username: data.username,
			email: data.email,
			password: encryptPassword(data.password),
			role: data.role,
		})
			.then((user) => {
				return Profile.create({}).then((profile) => {
					user.setProfile(profile);
					return Promise.resolve({
						...visibleProps(user.dataValues),
						token: user.getToken(),
					});
				});
			})
			.catch((err) => {
				return Promise.reject(
					err.errors.map((error) => ({
						field: error.path,
						reason: error.validatorKey,
					}))
				);
			});
	else {
		return Promise.reject(errors);
	}
};

const loginUser = (data) => {
	const uoe = data.usernameOrEmail;
	var loginData = {};
	if (uoe.includes('@')) loginData.email = uoe;
	else loginData.username = uoe;
	return User.findOne({ where: loginData }).then((user) => {
		if (user == null)
			return Promise.reject({
				status: 'error',
				reason: 'no such a username/email',
			});
		else {
			if (user.dataValues.password != encryptPassword(data.password))
				return Promise.reject({
					status: 'error',
					reason: 'wrong password',
				});
			else
				return Promise.resolve({
					status: 'ok',
					data: {
						...visibleProps(user.dataValues),
						token: user.getToken(),
					},
				});
		}
	});
};

function encryptPassword(password) {
	return password;
}
module.exports = {
	loginUser,
	createUser,
};
