const Sequelize = require('sequelize');
const jwt = require('jsonwebtoken');
const { jwtSecret } = require('../../consts');
const Model = Sequelize.Model;
class User extends Model {
	static getUserByToken(token) {
		try {
			return User.findOne({
				where: { id: jwt.verify(token, jwtSecret).id },
			});
		} catch (_e) {
			return null;
		}
	}
	getToken() {
		return jwt.sign(this.dataValues, jwtSecret);
	}
	static signupIsValid(data) {
		var messages = [];
		const userErr = validateUsername(data.username);
		if (userErr.length > 0) messages = [...messages, ...userErr];
		const emailErr = validateEmail(data.email);
		if (emailErr.length > 0) messages = [...messages, ...emailErr];
		return messages;
	}
}
schema = (sequelize) =>
	User.init(
		{
			username: {
				type: Sequelize.STRING,
				allowNull: false,
				unique: true,
			},
			email: {
				type: Sequelize.STRING,
				allowNull: false,
				unique: true,
			},
			password: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			role: {
				type: Sequelize.STRING,
				defaultValue: 'user',
			},
		},
		{ sequelize, modelName: 'user' }
	);

validateUsername = (username) => {
	if (username.includes('@'))
		return [{ field: 'username', reason: 'invalid username' }];
	else return [];
};
validateEmail = (email) => {
	if (email.includes('@')) return [];
	else return [{ field: 'email', reason: 'invalid email' }];
};

module.exports = schema;
