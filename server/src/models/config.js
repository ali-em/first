const Sequelize = require('sequelize');
const sequelize = new Sequelize('sare_sobh', 'root', 'root', {
	host: 'localhost',
	dialect: 'mysql',
	define: {
		charset: 'utf8',
		collate: 'utf8_general_ci',
	},
});

module.exports = sequelize;
