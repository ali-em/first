import { useRouter } from 'next/router';
import Layout from '../../../components/layout';
import Link from 'next/link';
import { server } from '../../config';
const Category = (props) => {
	const router = useRouter();

	return (
		<Layout>
			<h1>{props.shop.name}</h1>
			<p>{props.shop.description}</p>
			<hr />
			<h2>{props.product.name}</h2>
			<p>{props.product.price}</p>
		</Layout>
	);
};

Category.getInitialProps = async function ({ query }) {
	const name = query.name;
	const product = query.product;
	const res = await fetch(
		`${server}/api/v1/market/shops/${encodeURI(name)}/product/${encodeURI(
			product
		)}`
	);
	const data = await res.json();
	console.log('data', data);
	return {
		shop: data,
		product: data.products[0],
	};
};

export default Category;
