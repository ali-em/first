import { useRouter } from 'next/router';
import Layout from '../../components/layout';
import Link from 'next/link';
import fetch from 'isomorphic-unfetch';
import { server, images } from '../config';
import useAdmin from '../../components/useAdmin';
import DeleteShop from '../../components/deleteshop';
import { useEffect } from 'react';
import NotFound from '../../components/notfound';
import EditShop from '../../components/editshop';
const Shop = (props) => {
	const router = useRouter();
	const [hasRole, role] = useAdmin();
	useEffect(() => {}, []);

	let output =
		props.status == 'ok' ? (
			<>
				<h1>{props.shop.name}</h1>
				{role ? (
					<>
						<DeleteShop name={`${router.query.name}`} />
						<Link
							href="/admin/editshop/[name]"
							as={`/admin/editshop/${router.query.name}`}
						>
							<a>
								<button className="waves-effect waves-light btn-small">
									تغییر اطلاعات فروشگاه
								</button>
							</a>
						</Link>
						<Link
							href="/admin/shop/[id]/product/add"
							as={`/admin/shop/${props.shop.id}/product/add`}
						>
							<a>
								<button className="waves-effect waves-light btn-small">
									اضافه کردن محصول
								</button>
							</a>
						</Link>
					</>
				) : (
					''
				)}
				{props.shop.image ? (
					<div>
						<img
							src={`${server}/${props.shop.image.dest}`}
							style={{ width: 200 }}
							alt=""
						/>
					</div>
				) : (
					''
				)}
				<p>{props.shop.description}</p>
				<ul>
					{props.shop.products.map((p) => (
						<li key={p.id}>
							<Link
								href="/shop/[name]/[product]"
								as={`/shop/${router.query.name}/${p.id}`}
							>
								<a>{p.name}</a>
							</Link>
						</li>
					))}
				</ul>
			</>
		) : (
			<NotFound item="فروشگاه" />
		);
	return <Layout>{output}</Layout>;
};

Shop.getInitialProps = async function ({ query }) {
	const name = query.name;
	let res, data;
	try {
		res = await fetch(`${server}/api/v1/market/shops/${encodeURI(name)}`);
		data = await res.json();
	} catch (error) {
		alert('مشکلی پیش آمده');
	}

	console.log('data', data);
	return {
		status: data.status,
		shop: data.data,
	};
};

export default Shop;
