import Layout from '../components/layout';
import Link from 'next/link';
import globalStyles from '../styles/global';
import { server } from './config/index';
import fetch from 'isomorphic-unfetch';

const Index = (props) => (
	<Layout>
		<h1>Homepage</h1>
	</Layout>
);

Index.getInitialProps = async function () {
	const res = await fetch(`${server}/api/v1/market/shops`);
	const data = await res.json();

	console.log(`Show data fetched. Count: ${data.length}`);

	return {
		shops: data,
	};
};

export default Index;
