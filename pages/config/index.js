const dev = process.env.NODE_ENV !== 'production';
const localhost = 'http://localhost:3000';
const prodhost = 'https://your_deployment.server.com';
const host = dev ? localhost : host;
export const server = host;
export const api = host + '/api/v1';
export const images = host + '/images';
