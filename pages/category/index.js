import Layout from '../../components/layout';
import Link from 'next/link';
import { server } from '../config/index';
import fetch from 'isomorphic-unfetch';

const Category = (props) => (
	<Layout>
		<h1>Categories</h1>
		<ul>
			{props.categories.map((cat) => (
				<li key={cat.id}>
					<Link href="/category/[id]" as={`/category/${cat.id}`}>
						<a>{cat.name}</a>
					</Link>
				</li>
			))}
		</ul>
	</Layout>
);

Category.getInitialProps = async function () {
	const res = await fetch(`${server}/api/v1/market/shops/category`);
	const data = await res.json();
	console.log('data', data);
	return {
		categories: data,
	};
};

export default Category;
