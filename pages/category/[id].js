import { useRouter } from 'next/router';
import Layout from '../../components/layout';
import Link from 'next/link';
import { server } from '../config';
import NotFound from '../../components/notfound';
const Category = (props) => {
	const router = useRouter();
	let output =
		props.status == 'ok' ? (
			<ul>
				{props.shops.shops.map((shop) => (
					<li key={shop.id}>
						<Link href="/shop/[name]" as={`/shop/${shop.name}`}>
							<a>{shop.name}</a>
						</Link>
					</li>
				))}
			</ul>
		) : (
			<NotFound item="دسته بندی" />
		);
	return <Layout>{output}</Layout>;
};

Category.getInitialProps = async function ({ query }) {
	const id = query.id;
	const res = await fetch(`${server}/api/v1/market/shops/category/${id}`);
	const data = await res.json();
	console.log('data', data);
	return {
		status: data.status,
		shops: data.data,
	};
};

export default Category;
