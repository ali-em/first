import Layout from '../../components/layout';
import Link from 'next/link';
import { server } from '../config/index';
import fetch from 'isomorphic-unfetch';
import { useEffect, useState } from 'react';
import Router, { useRouter } from 'next/router';
const Admin = (props) => {
	const router = useRouter();
	const login = async () => {
		const username = document.getElementById('username').value;
		const password = document.getElementById('password').value;
		const body = JSON.stringify({
			usernameOrEmail: username,
			password,
		});
		console.log(body);
		let res = await fetch(`${server}/api/v1/user/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body,
		});
		let data = await res.json();
		if (data.status == 'ok') {
			localStorage.setItem('token', data.data.token);
			router.push('/admin');
		} else {
			alert(data.reason);
		}
	};
	useEffect(() => {}, []);
	return (
		<Layout>
			<div className="row">
				<div className="input-field col m6 s10 offset-s1 offset-m3">
					<input id="username" type="text" required />
					<label htmlFor="username">Username</label>
				</div>
			</div>
			<div className="row">
				<div className="input-field col m6 s10 offset-s1 offset-m3">
					<input id="password" type="text" required />
					<label htmlFor="password">password</label>
				</div>
			</div>
			<div className="row">
				<button
					role="button"
					onClick={login}
					className="waves-effect waves-light btn col s6 offset-s3 m2 offset-m5"
				>
					Login
				</button>
			</div>
		</Layout>
	);
};

export default Admin;
