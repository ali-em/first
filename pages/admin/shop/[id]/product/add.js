import fetch from 'isomorphic-unfetch';
import { api } from '../../../../config/index';
import Layout from '../../../../../components/layout';
import { useRouter } from 'next/router';
const AddProduct = (props) => {
	const router = useRouter();
	const createProduct = async (e) => {
		e.preventDefault();
		let form = document.getElementById('createproduct');
		let body = new FormData(form);
		const resp = await fetch(
			`${api}/market/shop/${router.query.id}/product`,
			{
				method: 'POST',
				headers: {
					token: localStorage.getItem('token'),
				},
				body,
			}
		);
		const data = await resp.json();

		if (data.status == 'ok') alert('اضافه شد');
		if (data.status == 'error') alert(data.error);
	};
	return (
		<Layout>
			<div
				style={{
					margin: 'auto',
					marginTop: 20,
					width: '50%',
					boxShadow: '1px 1px 3px 1px rgba(0,0,0,0.5)',
					padding: '20px 30px',
					boxSizing: 'content-box',
				}}
			>
				<h5 className="center-align">محصول جدید</h5>
				<h6 className="center-align">({router.query.id})</h6>
				<form method="post" id="createproduct" onSubmit={createProduct}>
					<div className="input-field col m6 s10 offset-s1 ">
						<input id="name" name="name" type="text" required />
						<label htmlFor="name">نام</label>
					</div>
					<div className="input-field col m6 s10 offset-s1 ">
						<textarea
							id="description"
							name="description"
							type="text"
						></textarea>
						<label htmlFor="description">توضیحات</label>
					</div>

					<div className="input-field col m6 s10 offset-s1 ">
						<input id="price" name="price" type="text" required />
						<label htmlFor="price">قیمت</label>
					</div>

					<div className="input-field col m6 s10 offset-s1 ">
						<input type="file" name="images" id="image" multiple />
						<label htmlFor="image">تصاویر</label>
					</div>

					<button
						role="button"
						onClick={createProduct}
						className="waves-effect waves-light btn col s6 m2 "
					>
						ثبت محصول
					</button>
				</form>
			</div>
		</Layout>
	);
};

export default AddProduct;
