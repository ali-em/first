import Layout from '../../components/layout';
import Link from 'next/link';
import { server } from '../config/index';
import fetch from 'isomorphic-unfetch';
import { useEffect, useState } from 'react';
import Router from 'next/router';
import AddCategory from '../../components/addcategory';
import AddShop from '../../components/addshop';
const Admin = (props) => {
	const checkLogin = async () => {
		const token = localStorage.getItem('token');
		if (token == undefined) {
			Router.push('/admin/login');
		}
		const res = await fetch(`${server}/api/v1/user/getrole`, {
			headers: {
				token: token,
			},
		});
		const role = (await res.json()).role;
		if (role != 'admin' && role != 'superadmin') {
			Router.push('/admin/login');
		}
	};
	useEffect(() => {
		checkLogin();
	}, []);
	const createCategory = () => {};
	return (
		<Layout>
			<AddCategory />
			<AddShop categories={props.categories} />
		</Layout>
	);
};
Admin.getInitialProps = async function ({ query }) {
	const res = await fetch(`${server}/api/v1/market/shops/category`);
	const data = await res.json();

	console.log('data', data);
	return {
		categories: data,
	};
};

export default Admin;
