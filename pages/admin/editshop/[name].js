import fetch from 'isomorphic-unfetch';
import { server } from '../../config/index';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Layout from '../../../components/layout';
const EditShop = (props) => {
	const router = useRouter();
	const [cats, setCats] = useState([]);
	const [shop, setShop] = useState({});
	const getCats = async () => {
		const res = await fetch(`${server}/api/v1/market/shops/category`);
		const data = await res.json();
		console.log('data', data);
		setCats(data);
	};
	const getShopdata = async () => {
		const name = router.query.name;
		console.log(name);
		let res, data;
		try {
			res = await fetch(
				`${server}/api/v1/market/shops/${encodeURI(name)}`
			);
			data = await res.json();
		} catch (error) {
			alert('مشکلی پیش آمده');
		}

		console.log('data', data);
		setShop(data.data);
	};
	useEffect(() => {
		if (router && router.query && router.query.name) {
			getCats();
			getShopdata();
		}
	}, [router]);
	const editShop = async (e) => {
		e.preventDefault();
		let form = document.getElementById('createshop');
		let data = new FormData(form);
		let cats = document.getElementsByClassName('categories');

		for (let i = 0; i < cats.length; i++) {
			if (cats[i].checked) data.append('categories', cats[i].value);
		}
		let res = await fetch(
			`${server}/api/v1/market/shop/${router.query.name}`,
			{
				method: 'PATCH',
				headers: {
					token: localStorage.getItem('token'),
				},
				body: data,
			}
		);
		let jres = await res.json();
		if (jres.status == 'ok') alert('Added successfully');
		else alert(`not added ${jres.data[0].field} ${jres.data[0].reason}`);
	};
	return (
		<Layout>
			<div
				style={{
					margin: 'auto',
					marginTop: 20,
					width: '50%',
					boxShadow: '1px 1px 3px 1px rgba(0,0,0,0.5)',
					padding: '20px 30px',
					boxSizing: 'content-box',
				}}
			>
				<h5 className="center-align"> تغییر اطلاعات فروشگاه</h5>
				<form method="post" id="createshop" onSubmit={editShop}>
					<div className="input-field col m6 s10 offset-s1 ">
						<input
							id="name"
							name="name"
							type="text"
							defaultValue={shop.name}
							required
						/>
						<label htmlFor="name">نام</label>
					</div>
					<div className="input-field col m6 s10 offset-s1 ">
						<textarea
							id="description"
							name="description"
							defaultValue={shop.description}
							type="text"
						></textarea>
						<label htmlFor="description">توضیحات</label>
					</div>
					<div className="input-field col m6 s10 offset-s1 ">
						<input
							id="phoneNumber"
							name="phoneNumber"
							type="text"
							defaultValue={shop.phoneNumber}
							required
						/>
						<label htmlFor="phoneNumber">شماره تلفن</label>
					</div>
					<div className="input-field col m6 s10 offset-s1 ">
						<input
							id="region"
							defaultValue={shop.region}
							name="region"
							type="text"
							required
						/>

						<label htmlFor="region">منطقه</label>
					</div>
					<div className="input-field col m6 s10 offset-s1 ">
						<textarea
							id="contact"
							defaultValue={shop.contact}
							name="contact"
							type="text"
						></textarea>
						<label htmlFor="contact">contact</label>
					</div>
					<div className="input-field col m6 s10 offset-s1 ">
						<input type="file" name="image" id="image" />
						<label htmlFor="image">تصویر</label>
					</div>
					<div className="input-field col m6 s10 offset-s1 row">
						<p>دسته ها</p>
						<div
							style={{
								backgroundColor: 'rgba(240, 240, 240, 1)',
								padding: 20,
							}}
						>
							{cats.map((cat) => (
								<label key={cat.id} style={{ margin: 5 }}>
									<input
										type="checkbox"
										className="categories"
										value={cat.id}
									/>
									<span>{cat.name}</span>
								</label>
							))}
						</div>
					</div>
					<button
						role="button"
						onClick={editShop}
						className="waves-effect waves-light btn col s6 m2 "
					>
						ثبت
					</button>
				</form>
			</div>
		</Layout>
	);
};

export default EditShop;
