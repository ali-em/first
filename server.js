const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const sequelize = require('./server/src/models/config');

const dev = process.env.NODE_ENV !== 'production';
const next = require('next');
const app = next({ dev });
const handle = app.getRequestHandler();
sequelize
	.sync({
		// force: true,
	})
	.then(() => {
		User.create({
			username: 'superadmin',
			password: 'superadmin',
			role: 'superadmin',
			email: 'emami1999@gmail.com',
		})
			.then((user) => console.log('created'))
			.catch((err) => console.log('not created'));
	});

const userRoutes = require('./server/src/routes/user');
const marketRoutes = require('./server/src/routes/market');

app.prepare().then(() => {
	const server = express();

	server.use(bodyParser.json());
	server.use(express.static('server/public'));

	server.use('/api/v1/user/', userRoutes);
	server.use('/api/v1/market/', marketRoutes);
	// Server-side

	server.get('*', (req, res) => {
		return handle(req, res);
	});

	/* eslint-disable no-console */
	server.listen(3000, (err) => {
		if (err) throw err;
		console.log('Server ready on http://localhost:3000');
	});
});
